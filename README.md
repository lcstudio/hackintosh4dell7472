# hackintosh4dell7472

## 项目介绍

2018年初，我终于获得了心仪的一款Dell 7472笔记本电脑(i5-8250/8G/256Gssd/uHD620)。
19年中愉快地升级了固态硬盘到512G(芝奇的MLC款)用了几年的High Sierra(10.13.6)之后终于被monterey更快速的启动（和水果公司放弃对HighSierra的支持）勾引到想升级了。虽说暂时因watcom手写板依然使用HighSierra，但是修订好的蓝牙和更稳定的USB使我喊出“OC真香”。

## 软件架构

### Hackintosh首先的是配置:

* cpu intel 8250u
* gpu uHD620 无独立显卡
* 声卡
* 网卡 Realtek 8168 千兆
* 固态 美光512G(MLC款) MTFDDAV512MBF-1AN15ABHA

参考大神配置:https://github.com/ic005k/DELL7472

fixed:
---

> 1. 主要碰到的问题是直接使用笔记本不断重启，参照之前clover诸如ig-platform-id修改后就正常了。
> 2. 大神smbios修订为iMac17,1本修订继续使用MacBookPro14,1,因为iMac17,1时外接HDMI显示器没响应

升级收获：
---

* 使用clover引导HighSierra使用手机USB共享网络有一定几率直接重启，切换到OC之后此问题不再重现

* CLOVER休眠唤醒之后蓝牙适配器不能正常接入鼠标和音箱，OC之后修复

* 总的来说OC 0.78版本值得尝试